package bendirs;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;

public class Main {

	private static Scanner reader;
	
	public static void main(String[] args) {
		reader = new Scanner(System.in); 
		String path = System.getProperty("user.dir");
		System.out.println("Your current path is " + path);
		System.out.print("Use current path (Y/N)? ");

		if (!getYesNo()){
			System.out.print("Enter path: ");
			path = reader.nextLine();
		}

		
		File dir = new File(path);
		while (!dir.exists() || !dir.isDirectory()){
			System.out.println("Not a path");
			System.out.print("Enter path or type exit: ");
			path = reader.nextLine();

			if (path.equals("exit")){
				System.out.println("Bye");
				return;
			}
			dir = new File(path);
		}
		
		System.out.println("Working on path " + path);
		
		
		File[] directories = dir.listFiles(new FileFilter() {
			  @Override
			  public boolean accept(File current) {
				return (current.isDirectory() && !current.isHidden());
			  }
		});
		
		for(File subDir : directories){
			if (subDir.exists() && !subDir.isHidden()){
				File[] subDirFiles = subDir.listFiles(new FileFilter(){
			        @Override
			        public boolean accept(File file) {
			            return (!file.isHidden() && !file.isDirectory());
			        }
			    });
				int count = subDirFiles.length;
		    	System.out.println("Directory " + subDir.getAbsolutePath() + " has " + count + " files inside");
		    	if (count == 1){
			    	System.out.print("Are you sure you want to move " + subDirFiles[0] + " to " + dir + "\\" + subDirFiles[0].getName() + " (Y/N)? ");
					if (getYesNo()){
						Path src = subDirFiles[0].toPath();
						Path dest = new File(dir, subDirFiles[0].getName()).toPath(); // "/dest/2014/a.txt"
						Path target = null;
						try {
							target = Files.move(src, dest);
						} catch (FileAlreadyExistsException e){
					    	System.out.print("File " + dest + " already exists. Overwrite (Y/N)? ");
					    	if (getYesNo()){
								try {
									target = Files.move(src, dest, StandardCopyOption.REPLACE_EXISTING);
								} catch (IOException e1) {
									System.out.println("ERROR");
									System.exit(0);
								}
					    	}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("ERROR");
							System.exit(0);
						}
						if (dest.equals(target)){
							System.out.println("Moved to " + target);
						}
					}
		    	}
			}
		}

		reader.close();
		System.out.println("Bye");


	}
	
	private static boolean getYesNo(){
		char c = reader.nextLine().charAt(0);
		
		while (c != 'y' && c != 'Y' && c != 'N' && c != 'n'){
			System.out.print("Wrong input, type Y/N: ");
			c = reader.nextLine().charAt(0);
		}
		
		if (c == 'y' || c == 'Y'){
			return true;
		} else if (c == 'N' || c == 'n'){
			return false;
		} else {
			System.out.println("ERROR");
			System.exit(0);
		}
		return false;
	}

}


